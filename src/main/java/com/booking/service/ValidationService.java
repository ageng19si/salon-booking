package com.booking.service;

import com.booking.models.Service;
import com.booking.models.Person;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

import java.util.List;

public class ValidationService {

    public static boolean validateCustomerId(String customerId) {
        List<Person> persons = PersonRepository.getAllPerson();
        return persons.stream()
                .filter(person -> person instanceof Person)
                .anyMatch(person -> person.getId().equals(customerId));
    }

    public static boolean validateEmployeeId(String employeeId) {
        List<Person> persons = PersonRepository.getAllPerson();
        return persons.stream()
                .filter(person -> person instanceof Person)
                .anyMatch(person -> person.getId().equals(employeeId));
    }

    public static boolean validateServiceId(String serviceId) {
        List<Service> services = ServiceRepository.getAllService();
        return services.stream()
                .anyMatch(service -> service.getServiceId().equals(serviceId));
    }
}
